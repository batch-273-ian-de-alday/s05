// alert("Welcome to Capstone Project")


// E-commerce

// Customer class
class Customer {
    constructor(email) {
        this.email = email;
        this.cart = new Cart();
        this.orders = [];
    }

    // checkout cart and push to orders
    checkout() {
        this.orders.push({products: this.cart.contents, totalAmount: this.cart.totalAmount});

    }
}

// Cart class
class Cart {
    constructor(contents) {
        this.contents = [];
        this.totalAmount = 0;
    }

    // add product to cart
    addtoCart(product, quantity) {
        this.contents.push({
            product: {name: product.name, price: product.price, isActive: product.isActive},
            quantity: quantity
        })
    }

    // Show cart contents
    showCartContents() {
        return this;
    }

    // update product quantity in cart
    updateQuantity(product, quantity) {
        this.contents.forEach(item => {
            if (item.product.name === product.name) {
                item.quantity = quantity;
            }
        })
    }

    // compute cart total amount
    computeTotalAmount() {
        this.contents.forEach(item => {
            this.totalAmount += item.product.price * item.quantity;
        })
    }

    // clear cart contents
    clearCart() {
        this.contents = [];
        this.totalAmount = 0;
    }
    
}

// Product class
class Product {
    constructor(name, price) {
        this.name = name;
        this.price = isNaN(price) ? undefined : price; // return undefined if price is not a number
        this.isActive = true;
    }

    // update product price
    updatePrice(newPrice) {
        this.price = newPrice;
        return this.price;
    }

    // archive product
    archive() {
        this.isActive = false;
        return this.isActive;
    }

    // unarchive product
    unarchive() {
        this.isActive = true;
        return this.isActive;
    }


}


console.log("Create Customer:")
const john = new Customer ("john@mail.com")
console.log(john)

console.log("Create Products:")
const prodA = new Product ("Soap", 9.99)
const prodB = new Product ("Shampoo", "awd")
const prodC = new Product ("Conditioner", 19.99)
const prodD = new Product ("Toothpaste", 4.99)


console.log(prodA)
console.log(prodB)
console.log(prodC)
console.log(prodD)

console.log("Update product price for prodB:")
prodB.updatePrice(10.99)
console.log(prodB)

console.log("Archive product prodC:")
prodC.archive()
console.log(prodC)

console.log("Unarchive product prodC:")
prodC.unarchive()
console.log(prodC)

console.log("Add product to cart:")
john.cart.addtoCart(prodA, 2)
console.log(john.cart)

console.log("Show cart contents:")
console.log(john.cart.showCartContents())

console.log("Update product quantity in cart:")
john.cart.updateQuantity(prodA, 3)
console.log(john.cart)

console.log("Compute cart total amount:")
john.cart.computeTotalAmount()
console.log(john.cart)

console.log("Checkout cart:")
john.checkout()
console.log(john)

console.log("Clear cart contents:")
john.cart.clearCart()
console.log(john.cart)

console.log("Johns cart after clearing cart contents:")
console.log(john)